Build Script For Docs:
======================
::

    sphinx-build -Q -b html ./Fabric-1.10.1/sites/docs ./Fabric-1.10.1/sites/_build/docs/
    sphinx-build -Q -b html ./Fabric-1.10.1/sites/www ./Fabric-1.10.1/sites/_build/www/
OR
::

    invoke www
    invoke docs -o -W
    invoke www -c -o -W


Add Files For Document:
=======================
::

    sites/
        docs/
            make.bat
            Makefile
        www/
            make.bat
            Makefile

Look up shell reason:
=====================
::

    $ which fab
    $ cat /usr/local/bin/fab >> ~/endpoint.py
