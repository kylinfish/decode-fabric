Create VirtualEnv:
------------------
virtualenv Fabric-Env


Install Fabric:
---------------
ln -s [your-path]/decode-fabric/Fabric-1.10.1/fabric/ ./Fabric-Env/lib/python2.7/site-packages/fabric


Start Python VirtualEnv:
------------------------
source ./Fabric-Env/bin/activate
pip install -r requirements.txt


Change File Mode:
-----------------
sudo chmod -R 755 ../
