# decode-fabric
decode-fabric is a package that learning process comments and record for source code.

# Why it was created?
在过去的时间里, 我频繁地阅读过一些框架组件的源码, 但是因为各种因素, 我并没有持续性地关注它, 使用它.
这种情况的结果虽然有助于暂时性地了解, 但在后续的使用中一样地遗忘, 显得很徒劳, 我不得不花时间去重新
了解它, 使用它, 理解它. 从它的源码, 从它的源码文档, 从它的官方文档, 而如果英语又不太流利的话, 其中
的曲折, 也只能"呵呵".

2015, 新的一年, 希望在语言的沟通上, 文字的写作上, 期冀新的发展, 新的突破.

基于以上因素, 这个decode项目将会一点点地积累, 沉淀, 发展, 直至玩烂.
